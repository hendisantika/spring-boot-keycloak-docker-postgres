## Lightweight RESTful API with Spring Boot, Keycloak, Docker, PostgreSQL, JPA, Lombok, Spotify plugin, OpenAPI, etc.

## How it works:

### **1. Docker. First, you need to install docker**

* Download Docker for Windows [Here](https://docs.docker.com/docker-for-windows/install/). Hint: Enable Hyper-V feature
  on windows and restart;
* Download Docker for MacOS [Here](https://docs.docker.com/desktop/install/mac-install/).
* Then open powershell and check:

```bash
docker info
```

or check docker version

```bash
docker -v
```

or docker compose version

```bash
docker-compose -v
```

### **2. Spring boot app**

* Clone the repository:

```bash
git clone https://gitlab.com/hendisantika/spring-boot-keycloak-docker-postgres.git
```

* Build the maven project:

```bash
mvn clean install
```

* Running the containers:

This command will build the docker containers and start them.

```bash
docker-compose up
```

or

This is a similar command as above, except it will run all the processes in the background.

```bash
docker-compose -f docker-compose.yml up
```

Appendix A.

All commands should be run from project root (where docker-compose.yml locates)

* If you have to want to see running containers. Checklist docker containers

```bash
docker container list -a
```

or

```bash
docker-compose ps
```