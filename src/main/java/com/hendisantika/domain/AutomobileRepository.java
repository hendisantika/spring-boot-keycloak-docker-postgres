package com.hendisantika.domain;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-keycloak-docker-postgres
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/7/22
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
@RepositoryRestResource
public interface AutomobileRepository extends JpaRepository<Automobile, Long> {
    List<Automobile> findByName(String name);

    List<Automobile> findByColor(String color);

    List<Automobile> findByNameAndColor(String name, String color);

    List<Automobile> findByColorStartsWith(String colorStartWith, Pageable page);
}
