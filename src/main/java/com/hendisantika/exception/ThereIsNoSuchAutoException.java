package com.hendisantika.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-keycloak-docker-postgres
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/7/22
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
//@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "There is no such automobile")
public class ThereIsNoSuchAutoException extends RuntimeException {
}
