package com.hendisantika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SpringBootKeycloakDockerPostgresApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootKeycloakDockerPostgresApplication.class, args);
    }

}
